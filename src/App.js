// import React from 'react';
import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  var mystyle={
    fontSize:50,
    color: '#000'
  }
  return (
    <div className="App">
      
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
       
        <div>
         <h1 style={mystyle} >Zender hook</h1>
       </div>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
     
    </div>
  );
}

export default App;
